# Normalize population of all cities using jq

```sh
normalize_population.jq <nhl23-24.json >nhl23-24-normalized.json
```
