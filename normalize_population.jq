#!/usr/bin/env -S jq -f

# Compute league average population (rounded to two decimal points) to $avgpop
([.teams[].pop] | add / length * 100 | round / 100 ) as $avgpop |
# Overwrite the current team population and season population to the league average
.teams[].pop = $avgpop |
.teams[].seasons[0].pop = $avgpop
